Rails.application.routes.draw do
  get 'report/make_report'
  get 'report/print_report'
  post 'report/post_report'
  resources :reports
  resources :notifications
  resources :messages
  resources :buys
  resources :products
  devise_for :clients
  devise_for :users
  resources :clients, only: [:index, :show]
  resources :users, only: [:show]
  get 'static_pages/home'
  get 'static_pages/benefits'
  get 'static_pages/trending'
  get 'static_pages/categories'
  get 'static_pages/sales'
  get 'static_pages/improvements'
  get 'static_pages/home'
  get 'static_pages/help'
  post 'static_pages/make_buy'
  root 'static_pages#home'
end
