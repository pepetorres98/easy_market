json.extract! product, :id, :title, :price, :description, :category, :stock, :width, :height, :depth, :client_id, :img, :created_at, :updated_at
json.url product_url(product, format: :json)
