json.extract! buy, :id, :client_id, :product_id, :quantity, :created_at, :updated_at
json.url buy_url(buy, format: :json)
