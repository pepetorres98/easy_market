class ReportController < ApplicationController
  def post_report
    if params[:type] == 'print'
      redirect_to action: "print_report", ticket: params[:ticket]
    elsif params[:type] == 'make'
      redirect_to action: "make_report", ticket: params[:ticket]
    elsif params[:type] == 'send'
      ClientMailer.with(ticket: params[:ticket], client: current_client).send_ticket.deliver!
      redirect_to action: "make_report", ticket: params[:ticket]
    else
      redirect_to :back, alert: "Incorrecto"
    end
  end

  def make_report
    @buy_client = Buy.where(client_id: current_client.id, ticket: params[:ticket])
  end

  def print_report
    @buy_client = Buy.where(client_id: current_client.id, ticket: params[:ticket])
    render :layout => false
    response.headers['Content-Type'] = 'text/plain'
    response.headers['Content-Disposition'] = 'attachment; filename=ticket.txt'
  end
end
