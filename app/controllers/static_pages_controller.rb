class StaticPagesController < ApplicationController
  def home
  end

  def benefits
  end

  def trending
  end

  def categories
  end

  def sales
  end

  def improvements
  end

  def make_buy
    buys = Buy.where(client_id: current_client.id, ticket: current_client.compras)
    buys.each do |element|
      product = Product.find(element.product_id)
      if product.stock > 1
        product.update(stock: product.stock-1)
      elsif product.stock == 1
        product.destroy
      end
    end
    client = Client.find(current_client.id)
    client.update(compras: client.compras+1)
    redirect_to products_path, alert: "¡Compra realizada con exito!"
  end
end
