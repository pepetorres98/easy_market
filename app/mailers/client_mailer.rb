class ClientMailer < ApplicationMailer
  default from: 'jotgatech@gmail.com'

  def send_ticket
    @buy_client = Buy.where(client_id: params[:client].id, ticket: params[:ticket])
    mail(to: params[:client].email, subject: "Ticket #{params[:ticket].to_i+1} - Easy Market")
  end
end
