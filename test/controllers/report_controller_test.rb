require 'test_helper'

class ReportControllerTest < ActionDispatch::IntegrationTest
  test "should get make_report" do
    get report_make_report_url
    assert_response :success
  end

  test "should get print_report" do
    get report_print_report_url
    assert_response :success
  end

end
