require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get static_pages_home_url
    assert_response :success
  end

  test "should get benefits" do
    get static_pages_benefits_url
    assert_response :success
  end

  test "should get trending" do
    get static_pages_trending_url
    assert_response :success
  end

  test "should get categories" do
    get static_pages_categories_url
    assert_response :success
  end

  test "should get sales" do
    get static_pages_sales_url
    assert_response :success
  end

  test "should get improvements" do
    get static_pages_improvements_url
    assert_response :success
  end

end
