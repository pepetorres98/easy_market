class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :title
      t.text :body
      t.integer :user_id
      t.integer :product_id

      t.timestamps
    end
  end
end
