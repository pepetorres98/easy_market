class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :title
      t.float :price
      t.text :description
      t.integer :category
      t.integer :stock
      t.float :width
      t.float :height
      t.float :depth
      t.integer :client_id
      t.string :img

      t.timestamps
    end
  end
end
