class CreateBuys < ActiveRecord::Migration[5.2]
  def change
    create_table :buys do |t|
      t.integer :client_id
      t.integer :product_id
      t.integer :quantity
      t.integer :ticket

      t.timestamps
    end
  end
end
